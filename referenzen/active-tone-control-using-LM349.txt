PARTS LIST FOR FIG.4 ACTIVE TONE CONTROL USING LM349

Resistor (all ¼-watt, ± 5% Carbon)
R1 – R4 = 10 KΩ
R5 = 1 KΩ

VR1, VR2 = 100 KΩ

Capacitors
C1, C4 = 0.0033 µF (Ceramic Disc)
C2, C3, C5 = 0.033 µF (Ceramic Disc)

C6, C7 = 0.1 µF (Ceramic Disc)

Semiconductor
IC1 = LM 349