# Preamplifier PHONO_01

scematic and pcb for a preamplifier based on the OPA 2134 PA ic.
    
    - RCA and balanced audio output
    - power supply max +/- 12V

based on: https://sound-au.com/project97.htm with a few modifications

<img src="preamp_PCB_front.png"
     style="float: left; margin-right: 10px;" />

<img src="preamp_PCB_naked_front.png"
     style="float: left; margin-right: 10px;" />

<img src="preamp_PCB_naked_back.png"
     style="float: left; margin-right: 10px;" />
